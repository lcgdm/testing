##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Dimitar Shiyachki <Dimitar.Shiyachki@cern.ch>
#
# This test requires one pool with two filesystems. Until multinode is supported,
# this has to be tested manually
#
##############################################################################
# meta: proxy=true
# meta: preconfig=../../DPM-config
# meta: manual=True

SCRIPTDIR="$(dirname "$(readlink -f ${BASH_SOURCE})")"
source "${SCRIPTDIR}/../../../Macros"

FNAME=bug_40553_file_`date +%s`
TOKEN=bug_40553_space_`date +%s`

test_bug40553_pre () {
   return 0
}

test_bug40553_post () {

   lcg-del -l -b -D srmv2 srm://$DPM_HOST:$SRMV2_PORT/srm/managerv2\?SFN=/dpm/cern.ch/home/$VO/$FNAME
   dpm-releasespace --token_desc $TOKEN
   rm -f bug40553

   return 0
}

python_script="

import dpm
import sys
result, pools = dpm.dpm_getpools()
if result :
   sys.exit(1)
for pool in pools :
   result, filesystems = dpm.dpm_getpoolfs(pool.poolname)
   if result :
      print pool.poolname
      sys.exit(2)
   fscount = 0
   for filesystem in filesystems :
      if filesystem.status == 0 :
         fscount += 1
   if fscount >= 2 :
      print pool.poolname
      sys.exit(0)
sys.exit(3)

"

test_bug40553 () {

   #
   # Find a pool that has at least two filesystems so that one can be drained
   # 

   poolname=$(python -c "$python_script")

   retcode=$?
   if [ $retcode -eq 1 ]; then
      echo "Error listing pools."
      return 1
   fi
   if [ $retcode -eq 2 ]; then
      echo "Error listing filesystems for pool $poolname."
      return 1
   fi
   if [ $retcode -eq 3 ]; then
      echo "No pools with at least 2 active filesystems were found. Drain cannot be tested."
      return 0
   fi

   echo "Pool selected: $poolname"

   #
   # Reserve space on the selected pool and create a volatile file
   #

   token_guid=$(dpm-reservespace --gspace 512M --poolname $poolname --token_desc $TOKEN)

   ret_code=$?
   if [ $ret_code -ne 0 ]; then
      echo "Unable to reserve space. Abort"
      return 1
   fi

   turl=$(./bug40553 srm://$DPM_HOST/dpm/cern.ch/home/$VO/$FNAME $token_guid)

   ret_code=$?
   if [ $ret_code -ne 0 ]; then
      echo "Unable to create a volatile file. Abort."
      return 1
   fi

   fshost=$(echo $turl | cut -f 1 -d ' ' | sed -e 's/gsiftp:\/\/\([^:/]*\).*/\1/')
      
   echo "dpm_disk selected for drain: $fshost"

   command="
      export DPM_HOST=localhost;
      export DPNS_HOST=localhost;
      export PATH=$PATH:/opt/lcg/bin;
      dpm-drain --server $fshost;"

   DPM_HOST_EXEC "$command"

   #
   # Check whether the drain completed successfully and the location of the file
   #

   ret_code=$?

   if [ $ret_code -eq 255 ]; then
      echo "ssh error"
      return 1
   elif [ $retcode -ne 130 ] && [ $retcode -ne 0 ]; then
      echo "dpm-drain exited with error code $ret_code"
      return 1
   fi

   gt_result=$(lcg-gt -b -D srmv2 -v srm://$DPM_HOST:$SRMV2_PORT/srm/managerv2\?SFN=/dpm/cern.ch/home/$VO/$FNAME gsiftp)
   new_host=$(echo $gt_result | grep gsiftp | sed -e 's/gsiftp:\/\/\([^:/]*\).*/\1/')
   req_id=$(echo $gt_result | grep -v gsiftp | head -1)

   if [ "$new_host" != "$fshost" ]; then
      echo "The volatile file was replicated to the new place."
      return 0
   else
      return 1
   fi
}

if (test_bug40553_pre); then
  if (test_bug40553); then
    if (test_bug40553_post); then
      TEST_PASSED
    else
      TEST_FAILED
    fi
  else
    TEST_FAILED
  fi
else
  TEST_FAILED
fi

