add_executable (bug40927 bug40927.c)
target_link_libraries (bug40927 ${DPM_LIBRARIES} dl)

install (TARGETS bug40927
         DESTINATION share/lcgdm/tests/regression/dpm/dpm-bug-40927)
install (PROGRAMS execute_test.sh
         DESTINATION share/lcgdm/tests/regression/dpm/dpm-bug-40927)

