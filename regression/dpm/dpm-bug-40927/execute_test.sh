##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Dimitar Shiyachki <Dimitar.Shiyachki@cern.ch>
#
##############################################################################
# meta: proxy=true
# meta: preconfig=../../DPM-config

SCRIPTDIR="$(dirname "$(readlink -f ${BASH_SOURCE})")"
source "${SCRIPTDIR}/../../../Macros"


test_bug40927_pre () {

   if dpns-ls -l /dpm/cern.ch/home/$VO/bug_40927_file 2> /dev/null | grep -q bug_40927_file; then
      echo "File found before running the test. Removing..."
      lcg-del -l srm://$DPM_HOST/dpm/cern.ch/home/$VO/bug_40927_file
   fi

   VO_virtgid=$(dpns-listgrpmap --group $VO | sed -e 's/\s*\([0-9]*\)\s.*/\1/')
   if [ $? -ne 0 ]; then
      echo "Unable to obtain the virtual group id for VO $VO"
      return 1
   fi
   return 0
}

test_bug40927_post () {

   if dpns-ls -l /dpm/cern.ch/home/$VO/bug_40927_file 2> /dev/null | grep -q bug_40927_file; then
      lcg-del -l srm://$DPM_HOST/dpm/cern.ch/home/$VO/bug_40927_file
   fi

   return 0
}

test_bug40927 () {

   #
   # Check whether the file exists in the namespace after a successfull DPM_ABORT
   # 


   ./bug40927 file:///bin/ls srm://$DPM_HOST/dpm/cern.ch/home/$VO/bug_40927_file 1 V 3600
   if [ $? -eq 0 ]; then
      if dpns-ls -l /dpm/cern.ch/home/$VO/bug_40927_file 2>/dev/null | grep -q bug_40927_file; then
         echo "File exists in namespace. Test failed."
         return 1
      else
         echo "Aborted file is not listed in the namespace."
      fi
   else
      echo "Aborting the DPM_PUT reqeust was not successfull. Test failed."
      return 1
   fi


   return 0
}

if test_bug40927_pre; then
  if test_bug40927; then
    if test_bug40927_post; then
      TEST_PASSED
    else
      TEST_FAILED
    fi
  else
    TEST_FAILED
  fi
else
  TEST_FAILED
fi

