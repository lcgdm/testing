##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Dimitar Shiyachki <Dimitar.Shiyachki@cern.ch>
#
##############################################################################
# meta: proxy=true
# meta: preconfig=../../LFC-config

SCRIPTDIR="$(dirname "$(readlink -f ${BASH_SOURCE})")"
source "${SCRIPTDIR}/../../Macros"

test_bug49498_pre () {

   if [ -z "${LFC_HOST}" ]; then
     echo "LFC_HOST not defined"
     return 1
   fi

   return 0
}

test_bug49498_post () {

   lfc-ls ${LFCDIR}/bug49498 > /dev/null 2>/dev/null
   if [ $? -eq 0 ]; then
      echo "Remove test directory $LFCDIR/bug49345"
      lfc-rm -r ${LFCDIR}/bug49345
      if [ $? -ne 0 ]; then
         echo "Cannot remove path $LFCDIR/bug49498 on LFC server: $LFC_HOST"
         return 1
      fi
   fi

   if [ "$POST_DELETE" == "yes" ]; then
     lfc-rm -r ${LFCDIR}
   fi

   return 0
}

test_bug49498 () {

   echo "Checking LFCDIR existence..."
   lfc-ls ${LFCDIR}

   if [ $? -ne 0 ]; then
      echo "LFCDIR not found on LFC server: $LFC_HOST"
      lfc-mkdir -p ${LFCDIR}
      if [ $? -ne 0 ]; then
        echo "Could not create $LFCDIR!"
        return 1
      fi
   fi

   result=$(lfc-mkdir -p lfn:${LFCDIR}/bug49498/non-existing/subdirectory 2>&1)
   if [ $? -eq 139 ]; then
      echo "lfc-mkdir segfaults on -p with lfn: prefix"
      return 1
   fi

   return 0
}

if (test_bug49498_pre); then
  if (test_bug49498); then
    if (test_bug49498_post); then
      TEST_PASSED
    else
      TEST_FAILED
    fi
  else
    TEST_FAILED
  fi
else
  TEST_FAILED
fi

