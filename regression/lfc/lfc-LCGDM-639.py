#!/usr/bin/env python
import lfc
import sys

# Initialize the file information
file = lfc.lfc_filereg()

file.lfn       = "/grid/dteam/lfc-LCGDM-639.regression"
file.guid      = "70657050-db23-11e1-8e75-dfd94382911c"
file.mode      = 0700
file.size      = 1234
file.sfn       = "srm://example.com/path/"
file.server    = "example.com"
file.csumtype  = "MD"
file.csumvalue = "1a31009319c99ebdefd23055b20ff034"

farray = [file,]

retCode = 0

# Remove if it was already there
lfc.lfc_delreplicasbysfn([file.sfn,], [""])

# Register
print "Registering file"
if lfc.lfc_registerfiles(farray)[0] != 0:
        print "Failed to register the file: %s" % lfc.sstrerror(lfc.cvar.serrno)
        sys.exit(1)

# Do the lfc_statr and check
statg = lfc.lfc_filestatg()
if lfc.lfc_statr(file.sfn, statg) != 0:
        print "Failed to do the lfc_statr: %s" % lfc.sstrerror(lfc.cvar.serrno)
        retCode = 1

if statg.csumtype != file.csumtype or statg.csumvalue != file.csumvalue or statg.guid != file.guid:
        print "Cns_statr repbuffer too small!"
        retCode = 1
else:
        print "Test for lfc_statr passed!!"

# Try with statg
if lfc.lfc_statg("", file.guid, statg) != 0:
	print "Failed to do the lfc_statg: %s" % lfc.sstrerror(lfc.cvar.serrno)
	retCode = 1

if statg.csumtype != file.csumtype or statg.csumvalue != file.csumvalue or statg.guid != file.guid: 
	print "Cns_statg repbuffer too small!"
	retCode = 1
else:
	print "Test for lfc_statg passed!"

# Clean
print "Removing file"
if lfc.lfc_delreplicasbysfn([file.sfn,], [""])[0] != 0:
        print "Failed to unlink the file: %s" % lfc.sstrerror(lfc.cvar.serrno)

sys.exit(retCode)

