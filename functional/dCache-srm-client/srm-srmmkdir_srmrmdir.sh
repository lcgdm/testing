#!/bin/bash
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2010.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHOR: Liudmila Stepanova, sli@inr.ru  
#
##############################################################################
#meta: proxy=true
#meta: preconfig=../../DPM-config
#
source  ../../Macros
PROXY_NEEDED
out=(`tr "." " " <<<$DPNS_HOST`)
   if [ ${#out[*]} -le 2 ];then
      echo $DPNS_HOST
      exit 2
   fi
DT=`date +%s`
RUN_COMMAND "srmmkdir -retry_num=2 -2 srm://$DPNS_HOST:$SRMV2_PORT/$DPNS_HOME/test.$DT"
if [ $? -ne 0 ]; then
   TEST_FAILED
fi

RUN_COMMAND "srmrmdir -retry_num=4 -2 srm://$DPNS_HOST:$SRMV2_PORT/$DPNS_HOME/test.$DT"
if [ $? -ne 0 ]; then
   TEST_FAILED
fi
TEST_PASSED
