#!/bin/bash
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Dimitar Shiyachki <Dimitar.Shiyachki@cern.ch>
#
##############################################################################
# meta: preconfig=../../DPM-config

SCRIPTDIR="$(dirname "$(readlink -f ${BASH_SOURCE})")"
source "${SCRIPTDIR}/../../Macros"
source fssel-common

NO_PROXY_NEEDED

error=0

TOKEN=t`date +%s`

STD_OUT=/tmp/stdout-`date +%s%N`
STD_ERR=/tmp/stderr-`date +%s%N`

echo "Test: Configure regular weights, all fs enabled"

# Use /etc/shift.conf to look up all trusted nodes
DISK_SERVERS=$(grep "DPM TRUST" /etc/shift.conf | sed -e 's/DPM TRUST //')
if [ -z "$DISK_SERVERS" ]; then
   TEST_FAILED "Disk servers lookup has failed"
fi

# Disable all available filesystems
change_allfs_status 1
if [ $? -ne 0 ]; then
   TEST_FAILED "Cannot disable existing filesystems to prepare the test environment"
fi

FS_SIZE=76
FS_COUNT=12
WPOOL_WEIGHTS=(7 2 16 9 6 7 4 2 1 4 1 1)
XPOOLNAME="wpool"

create_pool $XPOOLNAME $FS_COUNT $FS_SIZE

WEIGHT_SUM=0
for i in `seq 0 $[$FS_COUNT-1]`; do
   next_server=$(get_pool_server $XPOOLNAME $i)
   next_filesystem=$(get_pool_directory $XPOOLNAME $i)
   next_weight=$(get_fs_weight $XPOOLNAME $i)
   echo "Executing dpm-modifyfs --server ${next_server} --fs ${next_filesystem} --weight ${next_weight}"
   dpm-modifyfs --server ${next_server} --fs ${next_filesystem} --weight ${next_weight}
   if [ $? -ne 0 ]; then
      cleanup 0
      TEST_FAILED "Cannot configure fs weight using dpm-modifyfs"
   fi
   WEIGHT_SUM=$[$WEIGHT_SUM + ${next_weight}]
done

create_files $[$WEIGHT_SUM * 2] "file";
check_pool_sequence "wpool" "file" $[$WEIGHT_SUM * 2] $WEIGHT_SUM
ret=$?
remove_files $[$WEIGHT_SUM * 2] "file";
destroy_pool $XPOOLNAME

if [ $ret -ne 0 ]; then
   echo $ERROR
   TEST_FAILED
fi

echo "Test: OK"; echo;
cleanup 0
TEST_PASSED
