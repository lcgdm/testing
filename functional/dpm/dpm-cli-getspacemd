#!/bin/bash
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Dimitar Shiyachki <Dimitar.Shiyachki@cern.ch>
#
##############################################################################

SCRIPTDIR="$(dirname "$(readlink -f ${BASH_SOURCE})")"
source "${SCRIPTDIR}/../../Macros"

NO_PROXY_NEEDED

error=0

TOKEN1=t1`date +%s`
TOKEN2=t2`date +%s`
guid1=""
guid2=""
guid3=""

STD_OUT=/tmp/stdout-`date +%s%N`
STD_ERR=/tmp/stderr-`date +%s%N`

testName="Test 1:List all reserved spaces"

dpm-reservespace --gspace 15678000 --s_type P --token_desc $TOKEN1 >$STD_OUT 2>$STD_ERR
ret=$?
if [ $ret -ne 0 ]; then
   echo "$testName:dpm-reservespace returned $ret:FAILURE"
   REPORT_OUTPUT dpm-reservespace $STD_OUT $STD_ERR
   error=1
else
   guid1=$(cat $STD_OUT)
fi

if [ $error -ne 1 ]; then
   dpm-reservespace --gspace 15678000 --s_type V --token_desc $TOKEN2 >$STD_OUT 2>$STD_ERR
   ret=$?
   if [ $ret -ne 0 ]; then
      echo "$testName:dpm-reservespace(2) returned $ret:FAILURE"
      REPORT_OUTPUT "dpm-reservespace(2)" $STD_OUT $STD_ERR
      error=1
   else
      guid2=$(cat $STD_OUT)
   fi
fi

if [ $error -ne 1 ]; then
   dpm-reservespace --gspace 15678000 --s_type V >$STD_OUT 2>$STD_ERR
   ret=$?
   if [ $ret -ne 0 ]; then
      echo "$testName:dpm-reservespace(2) returned $ret:FAILURE"
      REPORT_OUTPUT "dpm-reservespace(2)" $STD_OUT $STD_ERR
      error=1
   else
      guid3=$(cat $STD_OUT)
   fi
fi


if [ $error -ne 1 ]; then
   dpm-getspacemd >$STD_OUT 2>$STD_ERR
   ret=$?
   if (grep -q "^$guid1 $TOKEN1" $STD_OUT) && (grep -q "^$guid2 $TOKEN2" $STD_OUT) && \
         (grep -q "^$guid3 " $STD_OUT) && [ $ret -eq 0 ]; then
      echo "$testName:dpm-getspacemd returned 0 all printed all defined spaces:SUCCESS"
   else
      echo "$testName:dpm-getspacemd failed:FAILURE"
      REPORT_OUTPUT dpm-getspacemd $STD_OUT $STD_ERR
      error=1
   fi
fi

testName="Test 2:List specific reserved space by GUID"

if [ $error -ne 1 ]; then
   dpm-getspacemd --token_desc $TOKEN1 >$STD_OUT 2>$STD_ERR
   if [ $? -eq 0 ] && grep -q "^$guid1 $TOKEN1" $STD_OUT; then
      echo "$testName:dpm-getspacemd returned 0 all printed reservation info:SUCCESS"
   else
      echo "$testName:dpm-getspacemd failed:FAILURE"
      REPORT_OUTPUT dpm-getspacemd $STD_OUT $STD_ERR
      error=1
   fi
else
   echo "$testName:Error preparing scenario:FAILURE"
fi

testName="Test 3:SI unit support"

if [ $error -ne 1 ]; then
   capacity=`dpm-getspacemd --si --space_token $guid1 |grep "ONLINE"|awk '{print $1}'`
   if [ $capacity == 15.68M ];then
      echo "$testName:dpm-getspacemd support correctly SI unit:SUCCESS"
   else
      echo "$testName:dpm-getspacemd does not support correctly SI unit:FAILURE"
      error=1
   fi  
else
   echo "$testName:Error preparing scenario:FAILURE"
fi

dpm-releasespace --space_token $guid1 >$STD_OUT 2>$STD_ERR
dpm-releasespace --space_token $guid2 >$STD_OUT 2>$STD_ERR
dpm-releasespace --space_token $guid3 >$STD_OUT 2>$STD_ERR

rm -f $STD_OUT
rm -f $STD_ERR

if [ $error -ne 0 ]; then
  TEST_FAILED
fi

TEST_PASSED

