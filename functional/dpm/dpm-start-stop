#!/bin/bash
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Martin Hellmich, CERN
#          Gianni Pucciani, CERN
#          Alejandro Álvarez Ayllón, CERN
#          Dimitar Shiyachki <Dimitar.Shiyachki@cern.ch>
# 
# Start stop script test
#
##############################################################################

SCRIPTDIR="$(dirname "$(readlink -f ${BASH_SOURCE})")"
source "${SCRIPTDIR}/../../Macros"

NO_PROXY_NEEDED

echo
echo "Target node is $DPM_HOST. Date: `date`"

DPM_START='/sbin/service dpm start'
DPM_STOP='/sbin/service dpm stop'
DPM_STATUS='/sbin/service dpm status'
# the init script has dpmmgr as user hardcoded,
# so why should the test not?
DPM_PGREP='pgrep -U dpmmgr dpm'
PYWEB_PGREP='ps -o pid,command | grep SimpleHTTPServe[r] | awk '"'"'{print $1}'"'"

error=0

dpm_stop() {
  DPM_HOST_EXEC "${DPM_STOP}"
  pid=`DPM_HOST_EXEC "${DPM_PGREP}"`
  echo "PID from stopped: $pid"
  if [ "x$pid" != "x" ]; then
    echo "dpm_stop: Error: DPM was not stopped"
  fi
}

dpm_start() {
  local message=$1
  if [ -z $message ] ; then
    message="DPM was not started"
  fi
  DPM_HOST_EXEC "${DPM_START}"
  pid=`DPM_HOST_EXEC "${DPM_PGREP}"`
  if [ "x$pid" == "x" ]; then
    echo "dpm_start: Error: $message"
  fi
}

testname="Test 1:make sure DPM is up"
echo "$testname [start]"
pid=`DPM_HOST_EXEC "${DPM_PGREP}"`
if [ "x$pid" == "x" ]; then
  echo "$testname: Error: DPM was never started"
  error=$(($error+1))
fi

testname="Test 2:starting a running DPM"
echo "$testname [start]"
# pid should stay the same, command should not fail
pid=`DPM_HOST_EXEC "${DPM_PGREP}"`
DPM_HOST_EXEC "${DPM_START}"
newpid=`DPM_HOST_EXEC "${DPM_PGREP}"`
echo "PIDs: $pid vs. $newpid"
if [ "x$pid" != "x$newpid" ]; then
  echo "$testname: Error: A running DPM was replaced by the start script"
  error=$(($error+1))
fi

testname="Test 3:stopping DPM"
echo "$testname [start]"
# DPM should stop, command should not fail
dpm_stop

testname="Test 4:test starting DPM"
echo "$testname [start]"
# DPM should start
dpm_start

testname="Test 5:kill -9 DPM; test start DPM"
echo "$testname [start]"
# DPM should start
pid=`DPM_HOST_EXEC "${DPM_PGREP}"`
DPM_HOST_EXEC "kill -9 ${pid}"
sleep 5 # make sure that DPM is stopped before reading a PID
newpid=`DPM_HOST_EXEC "${DPM_PGREP}"`
echo "Kill PIDs: $pid vs. $newpid"
if [ "x$newpid" != "x" ]; then
  if [ "$pid" != "$newpid" ]; then
    echo "$testname: Error: DPM was automatically restarted"
  else
    echo "$testname: Error: DPM was not killed"
  fi
  error=$(($error+1))
fi

DPM_HOST_EXEC "${DPM_START}"
pid=`DPM_HOST_EXEC "${DPM_PGREP}"`
if [ "x$pid" == "x" ]; then
  echo "$testname: Error: DPM was not started after a kill/crash"
  error=$(($error+1))
fi

testname="Test 6:stop DPM; occupy DPM port 5015; test start"
echo "$testname [start]"
# DPM should not start
dpm_stop
echo "Start a python webserver to occupy port 5015"
DPM_HOST_EXEC "nohup python -m SimpleHTTPServer 5015 > /dev/null &"
sleep 1
pypid=`DPM_HOST_EXEC "${PYWEB_PGREP}"`
DPM_HOST_EXEC "${DPM_START}"
start_return_val="$?"
DPM_HOST_EXEC "kill -9 $pypid"
if [ "$start_return_val" == "0" ]; then
  pid=`DPM_HOST_EXEC "${DPM_PGREP}"`
  if [ "x$pid" != "x" ]; then
    dpm_started='yes'
  else
    dpm_started='no'
  fi
  echo "$testname: Error: expected an error starting, because port 5015 is occupied, dpm started: ${dpm_started}"
  error=$(($error+1))
fi

testname="Test 7:stop DPM; occupy port 50150; test start"
echo "$testname [start]"
# DPM should start
dpm_stop
echo "Start a python webserver to occupy port 50150"
DPM_HOST_EXEC "nohup python -m SimpleHTTPServer 50150 > /dev/null &"
sleep 1
pypid=`DPM_HOST_EXEC "${PYWEB_PGREP}"`
DPM_HOST_EXEC "${DPM_START}"
start_return_val="$?"
DPM_HOST_EXEC "kill -9 $pypid"
if [ "$start_return_val" != "0" ]; then
  echo "$testname: Error: DPM did not start as expected. Port 50150 is occupied, which should not get in the way"
  error=$(($error+1))
fi

# make sure dpm is stopped
dpm_stop

# restart for further tests
dpm_start

# report failure or success
if [ $error -ne 0 ]; then
  TEST_FAILED
fi

TEST_PASSED
