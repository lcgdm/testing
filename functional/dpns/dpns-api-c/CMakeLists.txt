include_directories("${DPM_INCLUDE_DIR}/dpm")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D_REENTRANT")

add_executable (dpns_grpmap dpns_grpmap.c utils.c)
add_executable (dpns_usrmap dpns_usrmap.c utils.c)

target_link_libraries (dpns_grpmap ${DPM_LIBRARIES} dl m ssl)
target_link_libraries (dpns_usrmap ${DPM_LIBRARIES} dl m ssl)

install (TARGETS dpns_grpmap
                 dpns_usrmap
         DESTINATION share/lcgdm/tests/functional/dpns/dpns-api-c)

install (PROGRAMS execute_test.sh
         DESTINATION share/lcgdm/tests/functional/dpns/dpns-api-c)

