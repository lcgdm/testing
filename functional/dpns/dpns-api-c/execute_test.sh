#!/bin/bash
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Álvarez Ayllón, CERN
# 
# Wrapper for DPNS C API tests
#
##############################################################################
# meta: proxy=true
# meta: preconfig=../../DPM-config

SCRIPTDIR="$(dirname "$(readlink -f ${BASH_SOURCE})")"
source "${SCRIPTDIR}/../../../Macros"

PROXY_NEEDED

# Do something just to push a mapping
dpns-ls / &> /dev/null

NO_PROXY_NEEDED

# This script receives the C file name (without extension) as a parameter
# and uses the environment DPNS_HOST, DPNS_DOMAIN, DPNS_BASEDIR and VO, which should be set
ENV_REQUIRED DPNS_HOST DPNS_DOMAIN DPNS_BASEDIR VO

overall=0
for testFile in dpns_*
do
  testName=${testFile%.*}

  api_test_dir="/dpm/$DPNS_DOMAIN/$DPNS_BASEDIR/$VO/dpns-tests-$(date +%s)"

  dpns-mkdir $api_test_dir

  # Execute test
  echo "Executing $testName"
  ./$testName "$DPNS_HOST" "$api_test_dir"
  res=$?

  dpns-rm -r $api_test_dir

  if [ "$res" != 0 ]; then
    echo "FAILED"
    overall=1
  else
    echo "PASSED"
  fi
done

# End
if [ $overall -ne 0 ]; then
  TEST_FAILED
else
  TEST_PASSED
fi

