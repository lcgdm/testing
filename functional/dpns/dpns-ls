#!/bin/bash
# meta: proxy=True
# meta: preconfig=../../DPM-config
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Dimitar Shiyachki <Dimitar.Shiyachki@cern.ch>
#
##############################################################################

SCRIPTDIR="$(dirname "$(readlink -f ${BASH_SOURCE})")"
source "${SCRIPTDIR}/../../Macros"

PROXY_NEEDED

TEST_DIR=d`date +%s%N`
TEMP_FILE=/tmp/_dpm_test_temp
TEST_RESULT="FAILURE"

echo
echo "Working directory is: $DPNS_HOME"
echo
echo -n "Creating directory $TEST_DIR"
dpns-mkdir $TEST_DIR
if [ $? -eq 0 ]; then
   echo "...ok"
else
   echo "...fail"
fi

echo
for i in 1 2 3; do 
   echo -n "Creating file $TEST_DIR/bash_copy_$i"
   lcg-cp -v -b -D srmv2 file:/bin/bash srm://$DPM_HOST:$SRMV2_PORT/srm/managerv2\?SFN=/$DPNS_HOME/$TEST_DIR/bash_copy_$i
   if [ $? -eq 0 ]; then
      echo "...ok"
   else
      echo "...fail"
   fi
done

echo
echo "Listing $TEST_DIR"
dpns-ls -l $TEST_DIR > $TEMP_FILE
ret=$?
cat $TEMP_FILE 

if ( [ $ret -eq 0 ] ) && ( grep -q bash_copy_1 $TEMP_FILE; ) \
      && ( grep -q bash_copy_2 $TEMP_FILE; ) \
      && ( grep -q bash_copy_3 $TEMP_FILE; ) \
      && ( [ $(wc -l $TEMP_FILE | cut -f 1 -d ' ') -eq 3 ] ) then

   echo "Basic listing OK"

   TEST_SUBDIR=d`date +%s%N`

   echo
   echo -n "Creating subdirectory $TEST_SUBDIR"
   dpns-mkdir $TEST_DIR/$TEST_SUBDIR
   if [ $? -eq 0 ]; then
      echo "...ok"
   else
      echo "...fail"
   fi

   echo
   for i in 4 5 6 7; do
      echo -n "Creating file $TEST_DIR/$TEST_SUBDIR/bash_copy_$i"
      lcg-cp -v -b -D srmv2 file:/bin/bash srm://$DPM_HOST:$SRMV2_PORT/srm/managerv2\?SFN=/$DPNS_HOME/$TEST_DIR/$TEST_SUBDIR/bash_copy_$i
      if [ $? -eq 0 ]; then
         echo "...ok"
      else
         echo "...fail"
      fi
   done

   echo
   echo -n "Listing recursively $TEST_DIR"
   dpns-ls -l -R $TEST_DIR > $TEMP_FILE
   ret=$?
   cat $TEMP_FILE

   if ( [ $ret -eq 0 ] ) && \
         ( [ $(wc -l $TEMP_FILE | cut -f 1 -d ' ') -eq 12 ] ) then

      exists="yes"
      for i in 1 2 3 4 5 6 7; do
         if ! grep -q bash_copy_$i $TEMP_FILE; then
            exists="no"
         fi
      done

      if [ "$exists" == "yes" ]; then
         echo "Recursive listing OK"
         TEST_RESULT="SUCCESS"
      fi
   fi
fi

echo
for i in 1 2 3 4 5 6 7; do
   if [ $i -lt 4 ]; then
      echo -n "Erasing file $TEST_DIR/bash_copy_$i"
      lcg-del --nolfc -v -b -D srmv2 srm://$DPM_HOST:$SRMV2_PORT/srm/managerv2\?SFN=/$DPNS_HOME/$TEST_DIR/bash_copy_$i
      if [ $? -eq 0 ]; then
         echo "...ok"
      else
         echo "...fail"
      fi
   else
      echo -n "Erasing file $TEST_DIR/$TEST_SUBDIR/bash_copy_$i"
      lcg-del --nolfc -v -b -D srmv2 srm://$DPM_HOST:$SRMV2_PORT/srm/managerv2\?SFN=/$DPNS_HOME/$TEST_DIR/$TEST_SUBDIR/bash_copy_$i
      if [ $? -eq 0 ]; then
         echo "...ok"
      else
         echo "...fail"
      fi
   fi
done

echo
echo -n "Removing recursively directory $TEST_DIR"
dpns-rm -r $TEST_DIR
if [ $? -eq 0 ]; then
   echo "...ok"
else
   echo "...fail"
fi

echo
rm -f $TEMP_FILE

echo "OVERALL RESULT:"
if [ "$TEST_RESULT" == "FAILURE" ]; then
   TEST_FAILED
else
   TEST_PASSED
fi

