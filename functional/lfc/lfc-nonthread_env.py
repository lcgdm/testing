#!/usr/bin/env python
import _lfc
import sys

_lfc.lfc_setenv("LFC_HOST", "does.not.exist", 1)
s = _lfc.lfc_opendir("/")
if s is not None:
  print "Stat should have failed!!"
  sys.exit(1)

if _lfc.cvar.serrno == 0:
  print "serrno wasn't changed!!"
  sys.exit(1)

print "Call failed and serrno set to %d" % _lfc.cvar.serrno

