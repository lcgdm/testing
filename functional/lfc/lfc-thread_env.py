#!/usr/bin/env python
import _lfc
import socket
import sys
import threading
import time

# Thread implementation
class LfcThreadTest(threading.Thread):
  
  def __init__(self, host, expected):
    """Constructor"""
    threading.Thread.__init__(self)

    self.host     = host
    self.expected = expected
    self.failed   = False

  def run(self):
    try:
      _lfc.lfc_setenv('LFC_HOST', self.host, 1)
    except Exception, e:
      print >>sys.stderr, str(e)
      self.failed = True
      return
    # Wait to make sure the other thread executed the previous
    # statement
    time.sleep(2)

    if _lfc.lfc_getenv('LFC_HOST') != self.host:
      print "ENV(%s) != %s" % (_lfc.lfc_getenv('LFC_HOST'), self.host)
      self.failed = True
    else:
      print "ENV(%s) == %s" % (_lfc.lfc_getenv('LFC_HOST'), self.host)

    x = _lfc.lfc_opendir('/')

    if x is None:
      if _lfc.cvar.serrno == 0:
        print "The call failed but serrno wasn't properly set!"
        self.failed = True
      else:
        print "The call failed and serrno was properly set (%d)" % _lfc.cvar.serrno


    if x is None and self.expected:
      print "Expected a success, but failed!", self.host
      self.failed = True
    elif x is not None and not self.expected:
      print "Expected a failure, but succeeded!", self.host
      self.failed = True
    elif self.expected:
      print "Succeeded as expected:", self.host
    else:
      print "Failed, as expected:", self.host

# Main
if __name__ == "__main__":
  # First one match, so must succeed
  t1 = LfcThreadTest(socket.getfqdn(), True)
  # Second one doesn't, so must fail
  t2 = LfcThreadTest('does.not.exist', False)

  # Execute both
  t1.start()
  t2.start()

  t1.join()
  t2.join()

  # Fail if any one of those doesn't match
  if t1.failed or t2.failed:
    sys.exit(-1)
  else:
    sys.exit(0)

