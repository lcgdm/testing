#!/bin/bash
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# Author: Victor Galaktionov e-mail: victor.galaktionov@cern.ch
# Description of the test: Create LFC subdirectories for VO
#
# Input Parameters:
#       - VO name
#       - LFC host name
# Requisites:
#	- LFC server
#       - 1 configured VO
#
##############################################################################
# meta: preconfig=../../LFC-config
# meta: proxy=true

source ./lfc-common
SCRIPTDIR="$(dirname "$(readlink -f ${BASH_SOURCE})")"
source "${SCRIPTDIR}/../../Macros"

PROXY_NEEDED

access=( --- --x -w- -wx r-- r-x rw- rwx )

# Directory where we are going to test
main_dir="$LFCDIR/test-mkdir"

# Information
date
echo "Test LFC CLI mkdir"
echo "VO=$VO"
echo "Test main directory: $main_dir"


# Create directory
echo "SCENARIO: Directory doesn't exist"

echo "1. Create main directory: $main_dir"

lfc-mkdir -p $main_dir 2> $LFC_TEST_ERR
grep "File exists" $LFC_TEST_ERR &> /dev/null

if [ $? -eq 0 ]; then
   cat $LFC_TEST_ERR >&2
   TEST_FAILED "Directory exists"
fi

# Create subdirectories
UMAX=$DMAX
echo
echo "2. Create $UMAX subdirectories"
counter=0
dir=$main_dir
pdir=$main_dir

while [ $counter -lt $UMAX ]; do
  subdir="subdir-$counter"
  dir=$dir/$subdir
  echo "new dir:$dir"
  lfc-mkdir $dir
  lfc-ls $pdir > $LFC_TEST_TMP

  grep $subdir $LFC_TEST_TMP &> /dev/null
  
  if [ $? -ne 0 ]; then
    lfc_clear $main_dir
    TEST_FAILED "Failed to create $subdir"
  fi
  pdir=$dir
  let counter=counter+1
done
lfc_clear $main_dir

echo "Check all deletion result"
lfc-ls -l $main_dir &> /dev/null
if [ $? -eq 0 ]; then
  TEST_FAILED "The directory has been removed, but lfc-ls failed to report"
fi

# Create an existing directory
echo
echo "SCENARIO: Directory  already exists in the VO"
echo "Create main directory: $main_dir"

lfc-mkdir -p $main_dir &> /dev/null
if [ $? -ne 0 ]; then
  TEST_FAILED "Could not create $main_dir"
fi

lfc-mkdir $main_dir 2> $LFC_TEST_ERR
echo "Check diagn: cannot create ...  File exists"
grep "File exists" $LFC_TEST_ERR &> /dev/null

if [ $? == 1 ]; then
  lfc_clear $main_dir
  cat $LFC_TEST_ERR
  TEST_FAILED "The directory exists, but lfc-mkdir didn't report"
fi

echo "OK"

# Testing -p option
echo
echo "1. Scenario: test -p option"
cdir="$main_dir/sub1/sub2/sub3/sub4"

echo "1.1. Noncorrect request  mkdir for chain subdirectories"
lfc-mkdir "$cdir/sub5" 2> $LFC_TEST_TMP
grep "cannot create" $LFC_TEST_TMP > /dev/null
if [ $? == 1 ]; then 
    cat $LFC_TEST_TMP >&2
    lfc_clear $main_dir
    TEST_FAILED "Noncorrect -p option"
fi
echo "OK"

echo "1.2. Correct request with -p option"
lfc-mkdir -p "$cdir/sub5"
lfc-ls -l $cdir
lfc_clear $main_dir
echo "Check lfc-ls after remove main directory"
lfc-ls -l $main_dir &> /dev/null
if [ $? -eq 0 ]; then
  TEST_FAILED "The directory has been removed, but lfc-ls failed to report"
fi
echo "OK"

echo
echo "2. SCENARIO: check mode for make directory"

default="drwxrwxr-x"
echo "2.1. Check default mode: $default"

mask=`umask`
echo "Current umask: $mask"
umask 0002
echo "Changed to 0002"

echo "Create directory: $main_dir"
 
lfc-mkdir -p $main_dir
lfc-mkdir $main_dir/default-mode
lfc-ls -l $main_dir > $LFC_TEST_TMP

grep $default $LFC_TEST_TMP > /dev/null
if [ $? == 1 ]; then 
    echo "Created mode:" >&2
    cat $LFC_TEST_TMP >&2
    lfc_clear $main_dir
    TEST_FAILED "Noncorrect default mode, must be: $default"
fi
echo "OK"
lfc_clear $main_dir/default-mode

umask $mask
echo "Recovered previous umask"

echo "2.2.  Test check up 512 (000-777) variants of access mode for new subdirectory"
echo "SHORT=$SHORT, MODE=$MODE"

if [ $SHORT == 1 ]; then
    echo "Short mode: 000, 222, 333. 666, 777"
    cdir=$main_dir/short
    lfc-mkdir -m $MODE $cdir
    lfc_checkMode 0 0 0 $cdir
    lfc_checkMode 2 2 2 $cdir
    lfc_checkMode 3 3 3 $cdir
    lfc_checkMode 6 6 6 $cdir
    lfc_checkMode 7 7 7 $cdir
else
    echo "Long mode: all mode 000-nnn, n<$MODE"
    UMAX=$MODE
    
    owner=0
    cdir="$main_dir/qwerty"
    lfc-mkdir -m $MODE $cdir
    while [ $owner -lt $UMAX ]; do          	# cycle for owner access 
      group=0
      echo
      while [ $group -lt $UMAX ]; do      	# cycle for group access
        user=0
        echo
        while [ $user -lt $UMAX ]; do	# cycle for all users
          lfc_checkMode $owner $group $user $cdir
          let user=$user+1
        done
        let group=$group+1
      done
      let owner=$owner+1
    done
fi

lfc-chmod 777 $cdir
lfc_clear $cdir

echo
echo "Cleaning tasks"

rm -rf $LFC_TEST_TMP
rm -rf $LFC_TEST_ERR

lfc_clear $main_dir
lfc-ls -l $main_dir &> /dev/null
if [ $? -eq 0 ]; then
  echo "$main_dir has not been deleted!!"
  exit 1
fi

TEST_PASSED

