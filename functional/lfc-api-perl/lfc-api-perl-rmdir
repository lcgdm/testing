#!/usr/bin/perl -w
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2010.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Nikolay.Kutovskiy@jinr.ru
#
# DESCRIPTION:
# This test checks if the perl API lfc_rmdir function is working properly i.e.
# if it is able to remove a directory in the name server.
#
##############################################################################
# meta: proxy=True
# meta: preconfig=../../LFC-config
use strict;
use lfc;
use TestCommon;

my ($name,$res,$error,$err_num, $err_string);
test_title("lfc-rmdir");
# defining VO name
my $vo = $ENV{VO};

# initializing $error variable used for detecting test errors
$error = 0;

# checking the case when the directory exists
action_title("Checking the case when directory exists");
# defining unique dir name with help of current number of seconds since epoch
my $t = time();
$name = "/grid/$vo/perl_mkdir_test_"."$t";
# uncomment the line below to check if test prints the proper message in case of failure
#$name = "/perl_mkdir_test_"."$t";
$res = lfc::lfc_mkdir($name,0755);
if ($res != 0) {
  $err_num = $lfc::serrno;
  $err_string = lfc::sstrerror($err_num);
  action_failed();
  action_err_msg("There was an error during directory $name creation: Error $err_num ($err_string)");
  $error = 1;
} else {
  $res = lfc::lfc_rmdir($name);
  if ($res != 0) {
    $err_num = $lfc::serrno;
    $err_string = lfc::sstrerror($err_num);
    action_failed();
    action_err_msg("There was an error during directory $name deletion: Error $err_num ($err_string)");
    $error = 1;
  } else {
    action_ok();
  }
}

# checking the case when the named directory does not exist (ENOENT).
action_title("Checking the case when the named directory does not exist (ENOENT)");
$t = time();
$name = "/grid/$vo/perl_rmdir_ENOENT_test_$t";
# uncomment the next line to check if test prints the proper message in case of failure
#lfc::lfc_mkdir($name,0755);
$res = lfc::lfc_rmdir($name);
if ($res == 0) {
  action_failed();
  action_err_msg("Test could remove non-existing dir $name");
  $error = 1;
} else {
  action_ok();
  # uncomment three lines below to see if the error is correct
  #$err_num = $lfc::serrno;
  #$err_string = lfc::sstrerror($err_num);
  #print "Error $err_num ($err_string)\n";
}

# checking the case when the named directory is null pathname (ENOENT).
action_title("Checking the case when the named directory is a null pathname (ENOENT)");
$name = "";
# uncomment two lines below to check if test prints the proper message in case of failure
#$name = "/grid/$vo/perl_rmdir_ENOENT_test_$t";
#lfc::lfc_mkdir($name,0755);
$res = lfc::lfc_rmdir($name);
if ($res == 0) {
  action_failed();
  action_err_msg("Test could remove dir $name which name was a null pathname");
  $error = 1;
} else {
  action_ok();
  # uncomment three lines below to see if the error is correct
  #$err_num = $lfc::serrno;
  #$err_string = lfc::sstrerror($err_num);
  #print "Error $err_num ($err_string)\n";
}

# checking the case when a search permission is denied on a component of path prefix (EACCES)
action_title("Checking the case when a search permission is denied on a component of path prefix (EACCES)");
# defining unique dir name with help of current number of seconds since epoch
$t = time();
my $dir_1 = "/grid/$vo/perl_rmdir_EACCES_test_$t";
my $dir_2 = $dir_1."/test_dir";
$res = lfc::lfc_mkdir($dir_1,0755);
if ($res != 0) {
  $err_num = $lfc::serrno;
  $err_string = lfc::sstrerror($err_num);
  action_failed();
  action_err_msg("There was an error during directory $dir_1 creation: Error $err_num ($err_string)");
  $error = 1;
} else {
  # uncomment the line below to check if test prints the proper message in case of failure
  #lfc::lfc_chmod($dir_1,0444);
  $res = lfc::lfc_mkdir($dir_2,0755);
  if ($res != 0) {
    $err_num = $lfc::serrno;
    $err_string = lfc::sstrerror($err_num);
    action_failed();
    action_err_msg("There was an error during directory $dir_2 creation: Error $err_num ($err_string)");
    $error = 1;
  } else {
    lfc::lfc_chmod($dir_1,0644);
    # uncomment the line below to check if test prints the proper message in case of failure
    #lfc::lfc_chmod($dir_1,0755);
    $res = lfc::lfc_rmdir($dir_2);
    if ($res == 0) {
    action_failed();
    action_err_msg("Test could remove dir $dir_2 when a search permission was denied on a component of path prefix");
    $error = 1;
    } else {
      # uncomment three lines below to see if the error is correct
      #$err_num = $lfc::serrno;
      #$err_string = lfc::sstrerror($err_num);
      #print "Error: $err_num ($err_string)\n";
      action_ok();
      lfc::lfc_chmod($dir_1,0755);
      remove_dir($dir_2);
    }
  }
  # removing test dir $dir_1
  lfc::lfc_chmod($dir_1,0755);
  remove_dir($dir_1);
}

# checking the case when write permission on the parent directory is denied (EACCES)
$name = "/grid/$vo/";
action_title("Checking the case when write permission on the parent directory is denied (EACCES)");
# uncomment two lines below to check if test prints the proper message in case of failure
#$name = "/grid/$vo/perl_rmdir_EACCES_test_"."$t";
#lfc::lfc_mkdir($name,0755); 
$res = lfc::lfc_rmdir($name);
if ($res == 0) {
  action_failed();
  action_err_msg("The test could remove the directory $name while write permission on the parent directory was denied");
  $error = 1;
} else {
  # uncomment three lines below to see if the error is correct
  #$err_num = $lfc::serrno;
  #$err_string = lfc::sstrerror($err_num);
  #print "Error: $err_num ($err_string)\n";
  action_ok();
}

# checking the case when the path is NULL pointer (EFAULT).
action_title("Checking the case when the path is NULL pointer (EFAULT)");
$name = undef;
# uncomment two lines below to check if test prints the proper message in case of failure
#$name = "/grid/$vo/perl_rmdir_EFAULT_test_$t";
#lfc::lfc_mkdir($name,0755);
$res = lfc::lfc_rmdir($name);
if ($res == 0) {
  action_failed();
  action_err_msg("Test could remove dir $name while the path was NULL pointer");
  $error = 1;
} else {
  action_ok();
  # uncomment three lines below to see if the error is correct
  #$err_num = $lfc::serrno;
  #$err_string = lfc::sstrerror($err_num);
  #print "Error $err_num ($err_string)\n";
}

# checking the case when the named directory is not empty (EEXIST)
action_title("Checking the case when the named directory is not empty (EEXIST)");
# defining unique dir name with help of current number of seconds since epoch
$t = time();
$dir_1 = "/grid/$vo/perl_rmdir_EEXIST_test_$t";
$dir_2 = $dir_1."/test_dir";
$res = lfc::lfc_mkdir($dir_1,0755);
if ($res != 0) {
  $err_num = $lfc::serrno;
  $err_string = lfc::sstrerror($err_num);
  action_failed();
  action_err_msg("There was an error during directory $dir_1 creation: Error $err_num ($err_string)");
  $error = 1;
} else {
  # uncomment the line below to check if test prints the proper message in case of failure
  #lfc::lfc_chmod($dir_1,0444);
  $res = lfc::lfc_mkdir($dir_2,0755);
  if ($res != 0) {
    $err_num = $lfc::serrno;
    $err_string = lfc::sstrerror($err_num);
    action_failed();
    action_err_msg("There was an error during directory $dir_2 creation: Error $err_num ($err_string)");
    $error = 1;
  } else {
    $res = lfc::lfc_rmdir($dir_1);
    if ($res == 0) {
    action_failed();
    action_err_msg("Test could remove dir $dir_1 while it was not empty");
    $error = 1;
    } else {
      # uncomment three lines below to see if the error is correct
      #$err_num = $lfc::serrno;
      #$err_string = lfc::sstrerror($err_num);
      #print "Error: $err_num ($err_string)\n";
      action_ok();
      remove_dir($dir_2);
      remove_dir($dir_1);
    }
  }
}

# checking the case when a component of path prefix is not a directory (ENOTDIR)
action_title("Checking the case when a component of path prefix is not a directory (ENOTDIR)");
# creating a test file
$t = time();
$name = "/grid/$vo/perl_testfile_"."$t";
# uncomment the line below to check if test prints the proper message in case of failure
#$name = "/perl_filerename_test_"."$t";
my $guid = gen_guid();
$res = lfc::lfc_creatg($name,$guid,0664);
if ($res != 0) {
  $err_num = $lfc::serrno;
  $err_string = lfc::sstrerror($err_num);
  action_failed();
  action_err_msg("There was an error during file $name creation: Error $err_num ($err_string)");
  $error =1;
} else {
  $res = lfc::lfc_rmdir($name);
  if ($res == 0) {
    action_failed();
    action_err_msg("Test could remove file $name while a component of path prefix was not a directory");
    $error = 1;
  } else {
    action_ok();
    # uncomment three lines below to see if the error is correct
    #$err_num = $lfc::serrno;
    #$err_string = lfc::sstrerror($err_num);
    #print "Error $err_num ($err_string)\n";
  }
  remove_file($name);
}

# checking the case when the dir requested to be removed is a current working dir (EINVAL)
action_title("Checking the case when the dir requested to be removed is a current working dir (EINVAL)");
$t = time();
$name = "/grid/$vo/perl_rmdir_EINVAL_test_$t";
#my $dir_2 = $dir_1."/test_dir";
$res = lfc::lfc_mkdir($name,0755);
if ($res != 0) {
  $err_num = $lfc::serrno;
  $err_string = lfc::sstrerror($err_num);
  action_failed();
  action_err_msg("There was an error during directory $name creation: Error $err_num ($err_string)");
  $error = 1;
} else {
  $res = lfc::lfc_chdir($name);
  if ($res != 0) {
    $err_num = $lfc::serrno;
    $err_string = lfc::sstrerror($err_num);
    action_failed();
    action_err_msg("There was an error during an attempt to change current directory to $name: Error $err_num ($err_string)");
    $error = 1;
  } else {
    # trying to remove current working dir
    # uncomment the line below to check if test prints the proper message in case of failure
    #lfc::lfc_chdir("/grid/$vo");
    $res = lfc::lfc_rmdir($name);
    if ($res == 0) {
      action_failed();
      action_err_msg("The current working directory $name has been deleted");
      $error = 1;
    } else {
      # uncomment three lines below to see if the error is correct
      #$err_num = $lfc::serrno;
      #$err_string = lfc::sstrerror($err_num);
      #print "Error $err_num ($err_string)\n";
      action_ok();
      lfc::lfc_chdir("/grid/$vo");
      remove_dir($name);
    }
  }
}

# checking the case when the length of specified path exceeds CA_MAXPATHLEN (ENAMETOOLONG)
action_title("Checking the case when the length of specified path exceeds CA_MAXPATHLEN (ENAMETOOLONG)");
$name = "/grid/$vo/perl_rmdir_ENAMETOOLONG_test_";
#print "the length of \$name ($name) is ",length($name),"\n";
while (length($name)<$lfc::CA_MAXPATHLEN+1) { $name = $name.'a' }
#print "CA_MAXPATHLEN: $lfc::CA_MAXPATHLEN\n";
#print "\$name: $name\n\$ length is ",length($name),"\n";
$res = lfc::lfc_rmdir($name);
if ($res == 0) {
  action_failed();
  action_err_msg("Test could remove dir with length larger than CA_MAXPATHLEN ($lfc::CA_MAXPATHLEN)");
  $error = 1;
} else {
  # uncomment three lines below to see if the error is correct
  #$err_num = $lfc::serrno;
  #$err_string = lfc::sstrerror($err_num);
  #print "Error: $err_num ($err_string)\n";
  action_ok();
}

# checking the case when the length of path component exceeds CA_MAXNAMELEN (ENAMETOOLONG)
action_title("Checking the case when the length of path component exceeds CA_MAXNAMELEN (ENAMETOOLONG)");
$name = "perl_rmdir_ENAMETOOLONG_test_";
#print "the length of \$name ($name) is ",length($name),"\n";
while (length($name)<$lfc::CA_MAXNAMELEN+1) { $name = $name.'a' }
$name = "/grid/$vo/".$name;
#print "CA_MAXNAMELEN: $lfc::CA_MAXNAMELEN\n";
#print "\$name: $name\n\$ length is ",length($name),"\n";
$res = lfc::lfc_chdir($name);
if ($res == 0) {
  action_failed();
  action_err_msg("Test could remove directory with length larger than CA_MAXNAMELEN ($lfc::CA_MAXNAMELEN)");
  $error = 1;
} else {
  # uncomment three lines below to see if the error is correct
  #$err_num = $lfc::serrno;
  #$err_string = lfc::sstrerror($err_num);
  #print "Error: $err_num ($err_string)\n";
  action_ok();
}

# checking the case when LFC host is unknown
action_title("Checking the case when LFC host is unknown (SENOSHOST)");
# retrieving default value for LFC_HOST env variable
my $lfc_host = $ENV{'LFC_HOST'};
#print "LFC_HOST is $lfc_host\n";
#changing the value for LFC_HOST environment variable
$t = time();
$ENV{'LFC_HOST'} = "random_lfc_host_".$t;
# uncomment the line below to check if test prints the proper message in case of failure
#$ENV{'LFC_HOST'} = $lfc_host;
$name = "/grid/$vo/";
# temporary closing STDERR to prevent error message to be displayed during that subtest execution
open (COPY_STDERR,">&STDERR");
close(STDERR) or die "Can't close STDERR: $!\n";
$res = lfc::lfc_rmdir($name);
# restoring STDERR
open (STDERR,">&COPY_STDERR")|| die "Can't restore STDERR";
if ($res == 0) {
  action_failed();
  action_err_msg("Test could remove dir $name on unknown LFC host ($ENV{'LFC_HOST'})");
  $error = 1;
} else {
  action_ok();
  # uncomment three lines below to see if the error is correct
  #$err_num = $lfc::serrno;
  #$err_string = lfc::sstrerror($err_num);
  #print "Error $err_num ($err_string)\n";
}


# printing the test final status
if ($error) {
  test_failed();
} else {
  test_passed();
}
