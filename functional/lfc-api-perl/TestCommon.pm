#!/usr/bin/perl 

package TestCommon;
require Exporter;
our @ISA = ("Exporter");
our @EXPORT = qw(gen_guid test_title test_passed test_failed action_title action_ok action_failed action_err_msg remove_dir remove_file); 

use strict;
use lfc;
use Data::UUID;

# initializing $stat_pos variable used for fixing the status (OK or FAILED) result position in the line
my $status_pos = 120;

# defining VO name
my $vo = $ENV{VO};

# subroutine for generating GUID
sub gen_guid {
  my $_guid = new Data::UUID;
  my $guid = lc($_guid -> create_str());
  return $guid;
}

sub test_title {
  my $_test_title = $_[0];
  print "[$_test_title]\n";
}

sub test_passed {
  print "[PASSED]\n";
  exit 0;
}

sub test_failed {
  print "[FAILED]\n";
  exit -1;
}

# defining variable for the title of the performing action
my $title;
sub action_title {
  $title = $_[0];
  print "  * [ $title ]";
}

sub action_ok {
  my $title_length = length($title);
  printf "%*s",$status_pos-$title_length,"[  OK  ]\n";
}

sub action_failed {
  my $title_length = length($title);
  printf "%*s",$status_pos-$title_length,"[FAILED]\n";
}

sub action_err_msg {
  my $err_msg = $_[0];
  print "    [ERROR] $err_msg\n";
}

# removing test dir subroutine
sub remove_dir {
  my $name = $_[0];
  # uncomment the variable $name on the next line to check if test prints the proper message in case of failure
  #$name = "/";
  #&action_title("Removing test directory $name");
  my $res = lfc::lfc_rmdir($name);
  # checking if the test dir has been successfully removed
  if ($res !=0) {
    my $err_num = $lfc::serrno;
    my $err_string = lfc::sstrerror($err_num);
    #&action_failed();
    &action_err_msg("There was an error during directory $name deletion: Error $err_num ($err_string)");
  } else {
    #&action_ok();
  }
}

# removing test file subroutine
sub remove_file {
  my $name = $_[0];
  # uncomment the variable $name on the next line to check if test prints the proper message in case of failure
  #$name = "/";
  #&action_title("Removing test file $name");
  my $res = lfc::lfc_unlink($name);
  # checking if the test file has been successfully removed
  if ($res !=0) {
    my $err_num = $lfc::serrno;
    my $err_string = lfc::sstrerror($err_num);
    #&action_failed();
    &action_err_msg("There was an error during file $name deletion: Error $err_num ($err_string)");
  } else {
    #&action_ok();
  }
}

1;
