#!/bin/bash
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2011.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHOR: Fabrizio Furano, fabrizio.furano@cern.ch
#
##############################################################################
#meta: preconfig=../../LFC-config
source  ../../Macros

errornum=0
RUN_COMMAND "SEMsg_SendNotAvailable tcp://dev.msg.cern.ch:6166?wireFormat=openwire -t SEMsg_upstream srm://this/is/just/a/test"
if [ $? -ne 0 ]; then
 TEST_FAILED "the message could not be sent"
 else
 TEST_PASSED
fi
