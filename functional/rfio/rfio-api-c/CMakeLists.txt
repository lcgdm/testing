include_directories("${DPM_INCLUDE_DIR}/dpm")

add_executable(rfio-c-api     rfio-c-api.c)
add_executable(rfio-c-api-rcp rfio-c-api-rcp.c)

target_link_libraries (rfio-c-api     ${DPM_LIBRARIES} dl)
target_link_libraries (rfio-c-api-rcp ${DPM_LIBRARIES} dl)

install (PROGRAMS execute_test.sh
                  rfio-c-api.sh
                  rfio-c-api-rcp.sh
         DESTINATION share/lcgdm/tests/functional/rfio/rfio-api-c)
install (TARGETS rfio-c-api
                 rfio-c-api-rcp
         DESTINATION share/lcgdm/tests/functional/rfio/rfio-api-c)

