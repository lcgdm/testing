#!/bin/bash
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Alejandro Alvarez <aalvarez@cern.ch>
#
##############################################################################

# Run and check
mydomain=`echo $DPM_HOST | cut -d "." -f 2-`
mypath=/dpm/$mydomain/home/$VO
myfile="$mypath/testfile"`date +%s`

echo "Remote file: $myfile"

echo "Uploading..."
./rfio-c-api-rcp /etc/group $myfile
if [ $? -ne 0 ]; then
  echo FAILURE
  exit 1
fi

echo "Downloading..."
./rfio-c-api-rcp $myfile /tmp/hosts
if [ $? -ne 0 ]; then
  echo FAILURE
  exit 1
fi

echo "Diff..."
diff /etc/hosts /tmp/hosts
if [ $? -ne 0 ]; then
  echo FAILURE
  exit 1
fi

echo "SUCCESS"

