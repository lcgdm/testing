#include <stdio.h>
#include "rfio_api.h"

int main (int argc, char **argv)
{
        if (argc != 3) {
                fprintf (stderr, "usage: %s source destination\n", argv[0]);
                exit (1);
        }
        if (rfio_rcp (argv[1], argv[2], 0) < 0) {
                rfio_perror ("rfio_rcp");
                exit (1);
        }
        exit (0);
}


