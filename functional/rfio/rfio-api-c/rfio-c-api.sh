#!/bin/bash

if [ "x$1" = "x" ]; then
 if [ ! "x$DPM_HOST" = "x" ]; then
   node=$DPM_HOST
 else
  echo "  No target node defined ! Exiting."
 echo "   Usage: $0 <target node> <vo>"
  exit 1
 fi
else
 node=$1
fi

if [ "x$2" = "x" ]; then
  if [ ! "x$VO" = "x" ]; then
    vo=$VO
  else
     echo "   No vo specified ! Exiting."
     echo "   Usage: $0 <target node> <vo>"
     exit 1
  fi
else
 vo=$2
fi

echo "Target node is $node. `date`"
echo "Launching RFIO C API tests:"

mydomain=`echo $node | cut -d "." -f 2-`
mypath=/dpm/$mydomain/home/$vo/rfiocapitest$RANDOM

export DPM_HOST=$node
export DPNS_HOST=$node

./rfio-c-api $mypath

