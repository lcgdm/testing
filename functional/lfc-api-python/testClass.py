import commands
import os, sys

LFC_VO    = '/grid/' + os.environ['SAME_VO']
TEST_HOME = 'python_lfc_test'

class _test:
    def __init__(self):
        self.retVal=0
    def info(self):
        return "Implement me !!!"
    def test(self):
        print "Implement me"
    def ret(self):
        print "Implement me!!!"
    def getRetVal(self):
        return self.retVal
    def prepare(self):
        return True
    def clean(self):
        pass
    def get_guid(self):
        return commands.getoutput('uuidgen').split('\n')[0]

class _ntest(_test):
    def __init__(self):
      self.retVal=-1

class _testRunner:
  """
  Base implementation of the run() method
  """
  def __init__(self):
    self.tests = []
    self.name  = self.__name__

  def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            if testInstance.prepare() != False:
              ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
              testInstance.clean()
            else:
              ret1 = False
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal

#************* Interface for SAM and Python tests ***************
def SAM_Run(classDef):
  global LFC_VO
  print "Start test for adding a replica for a given file (lfc_addreplica)"
  print "1. Prepare environment"

  if len(sys.argv) > 1:
    os.environ['LFC_HOST'] = sys.argv[1]

  os.environ['LFC_HOME'] = os.environ['LFC_HOST'] + ":/grid/" + os.environ['SAME_VO']
  print "LFC_HOME:", os.environ['LFC_HOME']

  same_ok      = int(os.environ['SAME_OK'])
  same_err     = int(os.environ['SAME_ERROR'])
  same_warning = int(os.environ['SAME_WARNING'])

  print "2. Start test run()"

  instance = classDef()
  ret = instance.run()

  print "3. Ret test code: %s" % ret
  print "Exit"

  if (ret == True):
      print "Test is OK"
      sys.exit(same_ok)
  elif ret == False:
      print "Test is failure"
      sys.exit(same_err)
  else:
      print "Test is warning"
      sys.exit(same_warning)

