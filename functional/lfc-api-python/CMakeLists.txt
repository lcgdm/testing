install (PROGRAMS lfc-api-python-addreplica
                  lfc-api-python-addreplicax
                  lfc-api-python-chdir
                  lfc-api-python-creatg
                  lfc-api-python-del
                  lfc-api-python-delcomment
                  lfc-api-python-fsize
                  lfc-api-python-getacl
                  lfc-api-python-getcwd
                  lfc-api-python-getreplica
                  lfc-api-python-getreplicas
                  lfc-api-python-mkdir
                  lfc-api-python-mkdirg
                  lfc-api-python-opendir
                  lfc-api-python-opendirg
                  lfc-api-python-rename
                  lfc-api-python-rmdir
                  lfc-api-python-setacl
                  lfc-api-python-setatime
                  lfc-api-python-setcomment
                  lfc-api-python-setfsizeg
                  lfc-api-python-stat
                  lfc-api-python-statg
                  lfc-api-python-statr
                  lfc-api-python-statx
                  lfc-api-python-undelete
                  lfc-api-python-unlink
                  testClass.py
         DESTINATION share/lcgdm/tests/functional/lfc-api-python)

