#!/bin/sh
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2010.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHORS: Gianni Pucciani, CERN
#          Alejandro Álvarez Ayllón, CERN
#          Dimitar Shiyachki <Dimitar.Shiyachki@cern.ch>
# 
# Common stuff for lcgdm tests
#
##############################################################################

# Only root can perform this steps
if [ `whoami` != 'root' ]; then
	echo "User is not root, skipping."
else
	# Synchronization may be necessary
	/usr/sbin/ntpdate -u ip-time-1.cern.ch

  # System
  ARCH=`uname -m`
  RHVER=`cat /etc/redhat-release |  awk -F'release ' '{print $2}' | cut -c 1`

	# Location of site-info.def
	SITEINFO_DEF=/etc/yaim/site-info.def

	if [ "x$RHVER" == "x4" ]; then
		GLITE_VERSION="3.1"
	else
		GLITE_VERSION="3.2"
	fi
	if [ $ARCH == "i686" ]; then
		ARCH="i386"
	fi

	# Some needed configuration
	yes | /opt/glite/yaim/bin/yaim -r -f config_add_pool_env -n glite-SE_dpm_mysql -s $SITEINFO_DEF
	if [ ! -f /etc/profile.d/grid-env.sh ]; then
		echo "The links /etc/profile.d/grid-env.* have not been created!"
		return 1
	else
		echo "The links inside /etc/profile.d have been created"
	fi

	yes | /opt/glite/yaim/bin/yaim -r -f config_vomses -f config_vomsdir -n glite-SE_dpm_mysql -s $SITEINFO_DEF
	if [ $? -ne 0 ]; then
		echo "vomses can not be set"
		return 1
	fi

fi


# Set PATH
if [ -z `echo $PATH | grep /opt/glite/bin` ]; then
	export PATH=$PATH:/opt/glite/bin
fi
if [ -z `echo $PATH | grep /opt/lcg/bin` ]; then
	export PATH=$PATH:/opt/lcg/bin
fi
if [ -z `echo $PATH | grep /opt/globus/bin` ]; then
	export PATH=$PATH:/opt/globus/bin
fi
if [ -z `echo $PATH | grep /opt/d-cache/srm/bin/` ]; then
        export PATH=$PATH:/opt/d-cache/srm/bin/
fi
# For ETICS this might be necessary
if [ -z `echo $PATH | grep /usr/bin` ]; then
	export PATH=$PATH:/usr/bin
fi

# Python
if  [ "$ARCH" == "x86_64" ]; then
	if [ "x$RHVER" == "x4" ]; then
		export PYTHONPATH=/opt/lcg/lib64/python2.3/site-packages/:$PYTHONPATH
	else
		export PYTHONPATH=/opt/lcg/lib64/python2.4/site-packages/:$PYTHONPATH
	fi
else
	if [ "x$RHVER" == "x4" ]; then
		export PYTHONPATH=/opt/lcg/lib/python2.3/site-packages/:$PYTHONPATH
	else
		export PYTHONPATH=/opt/lcg/lib/python2.4/site-packages/:$PYTHONPATH
	fi
fi

# Perl
if [ "$ARCH" == "x86_64" ]; then
	SUFFIX="64"
else
	SUFFIX=""
fi
export PERL5LIB=$PERL5LIB:/opt/lcg/lib$SUFFIX/perl/

