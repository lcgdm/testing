#!/bin/sh
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2011.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHOR: Liudmila Stepanova, sli@inr.ru
#
##############################################################################
#meta: proxy=true
#meta: preconfig=../../LFC-config
source  ../../Macros
   PROXY_NEEDED
errornum=0
mydir=LFC-`date +%s`
LFCDIR=$LFC_BASE_DIR/$mydir
mod740="drwxr-----"
mod777="drwxrwxrwx"
RUN_COMMAND "lfc-mkdir -p -m 740 $LFCDIR"
if [ $? -ne 0 ]; then
 TEST_FAILED "directory is not created"
fi
array=(`lfc-ls -l $LFC_BASE_DIR |grep $mydir |awk '{print $1" " $3" " $4}'`)
echo [mod,uid,gid]=${array[0]},${array[1]},${array[2]} 
if [ "${array[0]}" != "$mod740" ]
   then
       errornum=1
fi  
RUN_COMMAND "lfc-chmod 777 $LFCDIR"
mod=`lfc-ls -l $LFC_BASE_DIR |grep $mydir|awk '{print $1}'`
if [ "$mod" != "$mod777" ]
   then
       errornum=1
       echo access mode of a LFC directory $mod != $mod777 
  else    
     echo access mode was successful changed from ${array[0]} to $mod
  fi
RUN_COMMAND "lfc-rm -r $LFCDIR"
if [ $errornum -eq 1 ]
   then
     TEST_FAILED
   else
    TEST_PASSED
fi
   
