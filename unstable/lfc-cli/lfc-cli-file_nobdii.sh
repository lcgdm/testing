#!/bin/sh
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2011.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHOR: Liudmila Stepanova, sli@inr.ru
#
##############################################################################
#meta: proxy=true
#meta: preconfig=../../LFC-config
source  ../../Macros
PROXY_NEEDED
errornum=0
myfile=/proc/cpuinfo
mod666="-rw-rw-rw-"
mod777="-rwxrwxrwx"
echo lcg-cr    -b --vo $VO -d $SURL/cpu_info  -D srmv2 file:$myfile 
lcg-cr -v -b --vo $VO -d $SURL/cpu_info  -D srmv2 file:$myfile > /tmp/guid  2> /tmp/out
if [ $? -ne 0 ]; then
TEST_FAILED
fi
guid=`cat /tmp/guid`
echo
echo guid=$guid
echo
rm -f /tmp/guid
crfile_path=`grep "Using LFN:" /tmp/out |awk -F: '{print $3}'`
echo $crfile_path
rm -f /tmp/out
path=`echo $crfile_path |awk -Ffile '{print $1}'`
crfile=`echo $crfile_path |awk -F/ '{print $NF}'`
array=(`lfc-ls -l $crfile_path | grep $crfile |awk '{print $1" " $3" " $4}'`) 
echo
echo mod,uid,gid=${array[0]},${array[1]},${array[2]}
echo
RUN_COMMAND "lfc-chmod 666 $crfile_path"
if [ $? -ne 0 ]
   then
    errornum=1
fi
mod=`lfc-ls -l $path | grep $crfile |awk '{print $1 }'`
if [ "$mod" != "$mod666" ]
   then
    errornum=1
fi
RUN_COMMAND "lfc-chmod 777 $crfile_path"
if [ $? -ne 0 ]
   then
    errornum=1
fi
mod1=`lfc-ls -l $path | grep $crfile |awk '{print $1 }'` 
echo
echo  "change modes: modes=$mod modesnew=$mod1"
echo
if [ "$mod1" != "$mod777"  ]
   then
    errornum=1
fi
echo "lfc-setcomment $crfile_path "it is cpu_info file""
lfc-setcomment $crfile_path " it is cpu_info file "
if [ "$?" -ne 0 ]
   then
    errornum=1
   else
   echo 
   echo Success
fi
echo "lfc-delcomment $crfile_path"
RUN_COMMAND "lfc-delcomment $crfile_path"
if [ $? -ne  0 ]
   then
    errornum=1
   fi
RUN_COMMAND "lcg-del -b -v -l -D srmv2 $SURL/cpu_info"
if [ $? -ne  0 ]
   then
    errornum=1
 fi
RUN_COMMAND "lcg-uf --vo dteam $guid $SURL/cpu_info"

lfc-ls -l $path |grep $crfile
if [ $? -eq 0 ]
   then
    errornum=1
fi
if [ $errornum == 1 ]
   then
    TEST_FAILED
fi
TEST_PASSED
