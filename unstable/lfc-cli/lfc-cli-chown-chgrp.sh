#!/bin/sh
##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2011.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# AUTHOR: Liudmila Stepanova, sli@inr.ru
#
##############################################################################
#meta: proxy=true
#meta: preconfig=../../LFC-config
source  ../../Macros
if [ `whoami` != 'root' ]; then
   owner=$VO
   PROXY_NEEDED
else
   owner="root"
fi
errornum=0
mydir=LFC-`date +%s`
LFCDIR=$LFC_BASE_DIR/$mydir
RUN_COMMAND "lfc-mkdir -p  $LFCDIR"
if [ $? -ne 0 ]; then
 TEST_FAILED "directory is not created"
fi
array=(`lfc-ls -l $LFC_BASE_DIR |grep $mydir |awk '{print $1" " $3" " $4}'`)
echo [mod,uid,gid]=${array[0]},${array[1]},${array[2]} 
let uidnew=${array[1]}+1
let guidnew=${array[2]}+1 
RUN_COMMAND "lfc-chown $uidnew:$guidnew $LFCDIR"
if [ $? -eq 0 ]   
  then  
      if [ "$owner" != "root" ]
        then
         errornum=1
         RUN_COMMAND "lfc-rm -r $LFCDIR"
         TEST_FAILED "command "lfc-chown $uidnew:$gidnew $LFCDIR" works with non root privilege"
        else 
         RUN_COMMAND "lfc-ls -l $LFC_BASE_DIR |grep $mydir "                 
       fi
  else
     if [ "$owner" = "root" ]
      then
        RUN_COMMAND "lfc-rm -r $LFCDIR"
        TEST_FAILED "lfc-chown $uidnew:$gidnew $LFCDIR don't work with root privilege"
     fi
fi        
RUN_COMMAND "lfc-chgrp $guidnew $LFCDIR"
if [ $? -eq 0 ]
  then
      if [ "$owner" != "root" ]
      then
         errornum=1
         RUN_COMMAND "lfc-rm -r $LFCDIR"
         TEST_FAILED "command "lfc-chown $uidnew:$gidnew $LFCDIR" works with non root privilege"
      else
         RUN_COMMAND "lfc-ls -l $LFC_BASE_DIR|grep $mydir"
       fi
  else
     if [ "$owner" = "root" ]
      then
        RUN_COMMAND "lfc-rm -r $LFCDIR"
        TEST_FAILED "lfc-chown $uidnew:$gidnew $LFCDIR don't work with root privilege"
     fi
fi

RUN_COMMAND "lfc-rm -r $LFCDIR"
if [ $errornum -eq 1 ]
   then
     TEST_FAILED
   else
    TEST_PASSED
fi
   
