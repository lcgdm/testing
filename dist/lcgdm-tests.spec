Name:           lcgdm-tests
Version:        1.9.0
Release:        2%{?dist}
Summary:        Testing package for lcgdm components
Group:          Applications/Internet
License:        ASL 2.0
URL:		http://svnweb.cern.ch/trac/lcgdm
Source0:	%{name}-%{version}.tar.gz
Buildroot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	cmake%{?_isa}
BuildRequires:	dpm-devel%{?_isa}
BuildRequires:	expat-devel%{?_isa}
BuildRequires:	git%{?_isa}
BuildRequires:	globus-gass-copy-devel%{?_isa}
BuildRequires:	wget%{?_isa}

Requires:	dpm-devel
Requires:	dpm-python
Requires: lcg-util 
Requires:	litmus
Requires: globus-gass-copy-progs 
Requires: nmap
Requires:	openldap-clients 
Requires: perl-lfc
Requires:	perl-XML-XPath
Requires: gcc
Requires: davix
Requires: gfal2-util
Requires: gfal2-all

%if %{?rhel}%{!?rhel:0} == 5
Requires: 	python26 
%else
Requires:	python
%endif
Requires: 	python-lfc 
Requires: 	uuid-perl 
Requires: 	voms-clients 

%description
The lcgdm-tests package provides functional, performance and regression tests
for all lcgdm componets.

%prep
%setup -q -n %{name}-%{version}

%build
cmake . \
	-DCMAKE_INSTALL_PREFIX=$RPM_BUILD_ROOT/usr \
	-DEMI=yes

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT

make install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/usr/share/lcgdm/*
