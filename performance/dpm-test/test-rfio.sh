#/bin/bash

mkdir -p results
for bschange in `cat bs.list`; do

cp /etc/shift.conf.orig /etc/shift.conf
echo "RFIO IOBUFSIZE "$bschange >> /etc/shift.conf
service rfiod restart

cat tests | while read testline
do
testname=`echo $testline | sed "s/\%.*//g"`
testname=`echo $testname"-"$bschange`
testcmd=`echo $testline | sed "s/.*\%//g"`
echo "#########"
echo $testname
echo "#########"
rm $testname
touch $testname
date +%R:%S > results/$testname
#for i in {1..20}; do
for filename in `ls -1 testfiles`
do
  	echo "Performing test with $filename"
        testexec=`echo $testcmd | sed "s/[$]file/$filename/g"`
        /usr/bin/time -o log -f %e $testexec
        echo "$filename `cat log`" >> results/$testname
#done
done
done
done
