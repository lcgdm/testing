#/bin/sh

for i in {1..10}; do
### 1MB
dd if=/dev/urandom of=testfiles/1mb-$i bs=1024 count=1024
### 10MB
dd if=/dev/urandom of=testfiles/10mb-$i bs=1024 count=10240
### 100MB
dd if=/dev/urandom of=testfiles/100mb-$i bs=1024 count=102400
### 500MB
dd if=/dev/urandom of=testfiles/500mb-$i bs=1024 count=512000
### 1GB
dd if=/dev/urandom of=testfiles/1gb-$i bs=1024 count=1048576
### 5GB
dd if=/dev/urandom of=testfiles/5gb-$i bs=1024 count=5242880
### 1.5MB
dd if=/dev/urandom of=testfiles/1.5mb-$i bs=1024 count=1536
### 50MB
dd if=/dev/urandom of=testfiles/50mb-$i bs=1024 count=51200
### 1.5GB
dd if=/dev/urandom of=testfiles/1.5gb-$i bs=1024 count=1572864
done

