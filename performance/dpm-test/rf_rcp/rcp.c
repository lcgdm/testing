#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <dirent.h>
#include "rfio_api.h"

#define OFNAME  "tests"
#define NEWNAME  "out.log"

int main(int argc, char **argv)
{	
	int i, j, k, n = 1, cnt = 0;
        char string[1000], buffer[BUFSIZ], *result, **words = NULL;
        FILE *fp, *ofp;
        unsigned int len = 0;
//      time_t Before, After;
//      long DeltaT;
        DIR *dp;
        struct dirent *dent;
        const char *DIRNAME = argv[1];
	
	struct timeval tv1,tv2,dtv;
        struct timezone tz;
        void time_start() { gettimeofday(&tv1, &tz); }
        long time_stop()
                { gettimeofday(&tv2, &tz);
                  dtv.tv_sec= tv2.tv_sec -tv1.tv_sec;
                  dtv.tv_usec=tv2.tv_usec-tv1.tv_usec;
                  if(dtv.tv_usec<0) { dtv.tv_sec--; dtv.tv_usec+=1000000; }
                  return dtv.tv_sec*1000+dtv.tv_usec/1000;
                }


	if (argc != 3) {
                fprintf (stderr, "Usage: %s source directory destination directory\n", argv[0]);
                return 1;
        }

		 
    	if( (dp = rfio_opendir(DIRNAME)) == NULL) {
        	fprintf(stderr, "opendir: %s: %s\n", DIRNAME, strerror(errno));
        	return 1;
    	}

	fprintf(stderr, "Opening directory %s\n", argv[1]);
  

	if( (ofp = fopen(NEWNAME, "w")) == NULL) {
                fprintf(stderr, "fopen: %s: %s\n", NEWNAME, strerror(errno));
                return 1;
        }

	fprintf(stderr, "Creating file %s\n", NEWNAME);

	while(dent = rfio_readdir(dp))
       		if(strcmp(".", dent->d_name) && strcmp("..", dent->d_name))
//			fprintf(stderr, "%s\n", dent->d_name);
//			fprintf(ofp, "%s\n", fp);
     			fprintf(ofp, "%s%s %s%s\n", argv[1],dent->d_name,argv[2],dent->d_name);
//	}
    	rfio_closedir(dp);
    	fclose(ofp);

	 if ((fp = fopen(NEWNAME, "rt")) == NULL)
        {
                fprintf (stderr, "Can not open file %s\n", NEWNAME, strerror(errno));
                return 1;
        }

	if ((ofp = fopen(OFNAME, "w")) == NULL)
        {
                fprintf (stderr, "Can not open file %s\n", OFNAME, strerror(errno));
                return 1;
        }

	while( !feof(fp) ) {
	
//	while(getline(&string,&len,fp)!= -1) {
		result = fgets(string,1000,fp);
		for ( result = strtok(string, " \n"); result != NULL; result = strtok(NULL, " \n") ){
			if ( ( words = (char**)realloc(words, sizeof(char*) * (cnt + 1)) ) == NULL ){
                        	fprintf(stderr, "No memory to new word!\n");
                        	return 1;
				}
				if ( ( words[cnt] = strdup(result) ) == NULL ){
                        		fprintf(stderr, "Can't duplicate word!\n");
                        		return 1;
                		}
				++cnt;
		}
	}
	
	fprintf(stderr, "Files for copy: %d\n", (cnt-1)/2);

	for ( i = 0; i < cnt-1; ++i){
                //fprintf(stderr, "Source #%d: %s\n", i+1, words[i]);
		for ( j = 0; j < cnt-1; ++j){
			j = i + 1;
			fprintf(stderr, "Source #%d: %s\n", n, words[i]);
			fprintf(stderr, "Destination #%d: %s\n", n, words[j]);
//			(void)time(&Before);
			time_start();
			if (rfio_rcp (words[i], words[j], 3600) < 0) {
	                	rfio_perror ("rfio_rcp");
				fprintf(ofp, "Error: %s \n", strerror(errno));
                        	return 1;
                	}
//			(void)time(&After);
//			DeltaT = After - Before;
//			fprintf(ofp, "Result: %s %d seconds.\n", words[j],DeltaT);
			fprintf(ofp, "Result: %s %d milliseconds.\n", words[j],time_stop());
			i = i + 1;
			++n;
			break;
		}
        }

	for ( i = 0; i < cnt; ++i ){
                if ( words[i] != NULL ){
                        free(words[i]);
                        words[i] = NULL;
                }
        }

	fclose(fp);
	free(words);
	words = NULL;
     	return 0;
}

