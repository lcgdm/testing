#/bin/bash

export PATH=$PATH:/opt/globus/bin:/opt/lcg/bin:/opt/glite/bin

if [ $# -lt 2 ]; then
  echo "Usage: $0 Source directory Destination directory"
  echo "Example: source_server:/path-to-files/ destination_server:/path-to-files/"
  exit 1
fi

# Need the binary
if [ ! -f "rcp" ]; then
  echo "The binary is not available. Need to compile."
  gcc -I/opt/lcg/include/dpm/ -L/opt/lcg/lib64/ -L/opt/lcg/lib/ -ldpm -ldl rcp.c -o rcp  
  if [ $? -ne 0 ]; then
    echo "Error in the compilation"
    exit 1
  fi
fi

# Get hosts
SOURCE_PATH=$1
DEST_PATH=$2

SOURCE_HOST=`echo $SOURCE_PATH | cut -d: -f 1`
DEST_HOST=`echo $DEST_PATH | cut -d: -f 1`

# Version specified?
V3=0
V2=0
if [ -z "$3" ] || [ "$3" == "v3" ]; then
	V3=1
fi
if [ -z "$3" ] || [ "$3" == "v2" ]; then
	V2=1
fi

# Some information to the user
echo "Source: ${SOURCE_HOST}"
echo "Destination: ${DEST_HOST}"

####################
# Common functions #
####################

# Creates a backup
# $1 is the host
# $2 is the path
function Backup() {
  echo "Backup $1:$2"

  scp root@"$1:$2" "$1:$2.orig"
  if [ $? -ne 0 ]; then
    echo "Can not backup $1:$2"
    exit 1
  fi
}

# Restores a backup and cleans
# $1 is the host
# $2 is the path (the original!)
# $3 is a service to restart (optional)
function Restore() {
  echo "Restoring $1:$2"

  scp root@"$1:$2.orig" "$1:$2"
  if [ $? -ne 0 ]; then
    echo "Can not restore $1:$2"
    exit 1
  fi
  ssh root@$1 "rm -rf $2.orig"
  if [ $? -ne 0 ]; then
    echo "Can not remove the backup of $2"
    exit 1
  fi
  if [ -n "$3" ]; then
    ssh root@$1 "service $3 restart"
    if [ $? -ne 0 ]; then
      echo "Can not restart $3"
      exit 1
    fi
  fi
}

# Change protocol V3 to V2 (or remain)
# $1 is the host
function EnableV2() {
  echo "Enabling V2 in $1"

  ssh root@$1 "sed -i 's/.*RFIO_READOPT.*//' /etc/sysconfig/rfiod"
  if [ $? -ne 0 ]; then
    echo "Can not execute sed over $1:/etc/sysconfig/rfiod"
    exit 1
  fi

  ssh root@$1 "service rfiod restart"
  if [ $? -ne 0 ]; then
    echo "Can't restart rfiod in $1"
    exit 1
  fi

  echo "Enabled V2"
}

# Change protocol V2 to V3
# $1 is the host
function EnableV3() {
  echo "Enabling V3 in $1"

  ssh root@$1 "echo -e \"RFIO_READOPT=16\nexport RFIO_READOPT\" >> /etc/sysconfig/rfiod"
  if [ $? -ne 0 ]; then
    echo "Can't append RFIO_READOPT to /etc/sysconfig/rfiod in $1"
    exit 1
  fi

  ssh root@$1 "service rfiod restart"
  if [ $? -ne 0 ]; then
    echo "Can't restart rfiod in $1"
    exit 1
  fi

  echo "Enabled V3"
}

############################
# Create test-wide backups #
############################
Backup $SOURCE_HOST /etc/sysconfig/rfiod
Backup $DEST_HOST   /etc/sysconfig/rfiod

################################################
# Test protocol V2 with different buffer sizes #
################################################

if [ $V2 -eq 1 ]; then

echo
echo "== Protocol V2 =="

# Use V2
# Shouldn't assume we are already in V2
EnableV2 $SOURCE_HOST
EnableV2 $DEST_HOST

# Backup of previous shift
Backup $SOURCE_HOST /etc/shift.conf
Backup $DEST_HOST   /etc/shift.conf

# Loop
for bschange in `cat bs.list`; do
  echo "V2 Buffer size $bschange"

  buffCmd='echo -e "`cat /etc/shift.conf | grep -v "RFIO IOBUFSIZE"`\nRFIO IOBUFSIZE '$bschange'" > /etc/shift.conf'

  # Source shift.conf
  ssh root@$SOURCE_HOST $buffCmd 
  if [ $? -ne 0 ]; then
    echo "Could not modify the RFIO IOBUFSIZE in $SOURCE_HOST"
    exit 1
  fi

  # Destination shift.conf
  ssh root@$DEST_HOST $buffCmd 
  if [ $? -ne 0 ]; then
    echo "Could not modify the RFIO IOBUFSIZE in $DEST_HOST"
    exit 1
  fi

  # Copy files
  ./rcp $SOURCE_PATH $DEST_PATH

  cp tests rf_rcp-$bschange &> /dev/null
  if [ $? != 0 ]; then
    echo "Can't copy tests"
  else
    echo "Copied tests"
  fi 

done

# Restore original shift.conf
Restore $SOURCE_HOST /etc/shift.conf
Restore $DEST_HOST   /etc/shift.conf

else
	echo "V2 disabled"
fi

################################################
# Test protocol V3 with different buffer sizes #
################################################

if [ $V3 -eq 1 ]; then
echo
echo "== Protocol V3 =="

# Enable V3
EnableV3 $SOURCE_HOST
EnableV3 $DEST_HOST

# Backup shift.conf
Backup $SOURCE_HOST /etc/shift.conf
Backup $DEST_HOST   /etc/shift.conf

# Loop
for bschange in `cat daemonv3.list`; do
  echo "V3 Buffer size $bschange"
  
  buffCmd='echo -e "`cat /etc/shift.conf | grep -v "RFIO DAEMONV3"`\nRFIO DAEMONV3_RDMT_BUFSIZE '$bschange'\nRFIO DAEMONV3_RDSIZE '$bschange'" > /etc/shift.conf'

  # Change source shift.conf
  ssh root@$SOURCE_HOST $buffCmd 
  if [ $? -ne 0 ]; then
    echo "Can not change shift.conf in $SOURCE_HOST"
    exit 1
  fi

  # Change destination shift.conf
  ssh root@$DEST_HOST $buffCmd
  if [ $? -ne 0 ]; then
    echo "Can not change shift.conf in $DEST_HOST"
    exit 1
  fi

  # Copy files
  ./rcp $1 $2
  if [ $? -ne 0 ]; then
    echo "Error copying"
    exit 1
  fi
  cp tests rf_rcp-v3-$bschange &> /dev/null

  if [ $? != 0 ]; then
    echo "Can't copy tests"
    exit 1
  fi

done

# Restore
Restore $SOURCE_HOST /etc/shift.conf
Restore $DEST_HOST   /etc/shift.conf
else
	echo "V3 disabled"
fi

################
# We are done! #
################
Restore $SOURCE_HOST /etc/sysconfig/rfiod rfiod
Restore $DEST_HOST   /etc/sysconfig/rfiod rfiod

echo "- TEST PASSED -"
exit 0

