#!/bin/bash

rm -f report.csv
rm -f /tmp/aux.csv

for i in rf_rcp-*; do
  suffix=${i:7}

  # Version and buffer size
  v3=${suffix:0:2}
  if [ $v3 == "v3" ]; then
    version="V3"
    size=${suffix:3}
  else
    version="V2"
    size=$suffix
  fi

  # Dump to a temporal file
  echo "Dumping $i as $version (Buffer $size)"
  cat $i | grep -oP "\d+(\.\d+)?((mb)|(gb))-\d+\s\d+" | awk "{\$0=\"$version $size \"\$0; print \$0}" | sed "s/ /,/g" | sed "s/-[0-9]*//g" >> /tmp/aux.csv

done

# Sort so the grouping works
sort /tmp/aux.csv -o /tmp/aux-sorted.csv
rm -f /tmp/aux.csv

# Dump the temporal to the definitive getting the average
group_label=""
while read line; do
  label=`echo $line | cut -d ',' -f 1,2,3`
  val=`echo $line | cut -d ',' -f 4`

  if [ "$label" != "$group_label" ]; then

    if [ "$group_label" != "" ]; then
      avg=$((group_total / group_count))
      echo "${group_label},${avg}" >> report.csv
    fi

    group_label=$label
    group_total=0
    group_count=0
  fi

  group_total=$((group_total + val))
  group_count=$((group_count + 1))

done < /tmp/aux-sorted.csv

# Last
avg=$((group_total / group_count))
echo "${group_label},${avg}" >> report.csv

