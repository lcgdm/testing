#/bin/bash

if [ -n "$1" ]; then
	testFile=$1
else
	testFile="tests"
fi

mkdir -p results
cat $testFile | while read testline
do
testname=`echo $testline | sed "s/\%.*//g"`
testcmd=`echo $testline | sed "s/.*\%//g"`
echo "#########"
echo $testname
echo "#########"
rm results/$testname
touch results/$testname
#for i in {1..20}; do
for filename in `ls -1 testfiles`
do
  	echo "Performing test with $filename"
        testexec=`echo $testcmd | sed "s/[$]file/$filename/g"`
        /usr/bin/time -o log -f %e $testexec
        echo "$filename `cat log`" >> results/$testname
#done
done
done
