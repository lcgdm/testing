#
# This module detects if neon is installed and determines where the
# include files and libraries are.
#
# This code sets the following variables:
# 
# NEON_LIBRARIES   = full path to the neon libraries
# NEON_INCLUDE_DIR = include dir to be used when using the neon library
# NEON_VERSION_*   = Neon version
# NEON_FOUND       = set to true if neon was found successfully
#
# NEON_LOCATION
#   setting this enables search for dpm libraries / headers in this location


# -----------------------------------------------------
# NEON Libraries
# -----------------------------------------------------
find_library(NEON_LIBRARIES
    NAMES neon
    HINTS ${NEON_LOCATION}/lib ${NEON_LOCATION}/lib64 ${NEON_LOCATION}/lib32
    DOC "Libneon"
)

# -----------------------------------------------------
# Neon Include Directories
# -----------------------------------------------------
find_path(NEON_INCLUDE_DIR 
    NAMES neon/ne_session.h 
    HINTS ${NEON_LOCATION} ${NEON_LOCATION}/include ${NEON_LOCATION}/include/*
    DOC "The neon include directory"
)
if(NEON_INCLUDE_DIR)
    set (NEON_INCLUDE_DIR "${NEON_INCLUDE_DIR}/neon")
    message(STATUS "neon includes found in ${NEON_INCLUDE_DIR}")
endif()

# -----------------------------------------------------
# Neon version
# -----------------------------------------------------
if (NEON_LIBRARIES)
  execute_process(COMMAND neon-config --version
                  OUTPUT_VARIABLE NEON_VERSION)
                  
  string(REGEX MATCH "[0-9]+.[0-9]+.[0-9]+" NEON_VERSION ${NEON_VERSION})
  string(REPLACE "." ";" NEON_VERSION_LIST ${NEON_VERSION})
  list(GET NEON_VERSION_LIST 0 NEON_VERSION_MAJOR)
  list(GET NEON_VERSION_LIST 1 NEON_VERSION_MINOR)
  list(GET NEON_VERSION_LIST 2 NEON_VERSION_REV)
  
  message(STATUS "neon found with version ${NEON_VERSION_MAJOR}.${NEON_VERSION_MINOR}")
endif()

# -----------------------------------------------------
# handle the QUIETLY and REQUIRED arguments and set DPM_FOUND to TRUE if 
# all listed variables are TRUE
# -----------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(neon DEFAULT_MSG NEON_LIBRARIES NEON_INCLUDE_DIR)
mark_as_advanced(NEON_INCLUDE_DIR NEON_LIBRARIES NEON_VERSION NEON_VERSION_MAJOR NEON_VERSION_MINOR NEON_VERSION_REV)

